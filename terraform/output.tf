output "vpc-id" {
  value = aws_vpc.vpc.id
}
output "igw-id" {
  value = aws_internet_gateway.igw.id
}
output "public-subnet-id" {
  value = aws_subnet.public.*.id
}
output "private-subnet-id" {
  value = aws_subnet.private.*.id
}
output "controller-instance-id" {
  value = aws_instance.main.id
}
output "server-instances-id" {
  value = aws_instance.consul_server.*.id
}
output "client_api_instance-id" {
  value = aws_instance.consul_client_api.*.id
}
output "client_web_instance-id" {
  value = aws_instance.consul_client_web.*.id
}