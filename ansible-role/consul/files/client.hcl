data_dir = "/opt/consul"

client_addr = "0.0.0.0"

server = false

bind_addr = "0.0.0.0"

advertise_addr = "{{ GetInterfaceIP `eth0` }}"

encrypt_verify_incoming = true

encrypt_verify_outgoing = true

verify_incoming = true

verify_outgoing = true

verify_server_hostname = true

ca_file = "/etc/consul.d/certs/consul-agent-ca.pem"

cert_file = "/etc/consul.d/certs/dc1-server-consul-0.pem"

key_file = "/etc/consul.d/certs/dc1-server-consul-0-key.pem"

