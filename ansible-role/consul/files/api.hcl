service {
  name = "api"
  port = 9090
  # token = "" # put api service token here
  check {
    id = "api"
    name = "HTTP API on Port 9090"
    http = "http://localhost:9090/health"
    interval = "30s"
  }
}