client_addr = "0.0.0.0"
bind_addr = "0.0.0.0"
advertise_addr = "{{ GetInterfaceIP `eth0` }}"
data_dir =  "/consul/data"
node_name = "consul-client1"
retry_join = ["consul-server"]
encrypt = "Ei7HwAc/oqX62Cis4UWG2uO1X2g2Ani8SAI8wS22Z1w="
encrypt_verify_incoming = true
encrypt_verify_outgoing = true
verify_incoming = true
verify_outgoing = true
verify_server_hostname = true
ca_file   = "/consul/config/certs/consul-agent-ca.pem"
cert_file = "/consul/config/certs/dc1-server-consul-0.pem"
key_file  = "/consul/config/certs/dc1-server-consul-0-key.pem"



