# Creating VPC
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(
    { "Name" = "${var.main_tag}-vpc" },
    { "Project" = var.main_tag },
    { "Owner" = var.owner_tag },
  )
}

# Creating IGW 
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = merge(
    { "Name" = "${var.main_tag}-igw" },
    { "Project" = var.main_tag },
    { "Owner" = var.owner_tag },
    var.igw_tags
  )
}
# Elastic IP for NAT gateway
resource "aws_eip" "eip" {
  vpc = true
  tags = merge(
    { "Name" = "${var.main_tag}-elasticIP" },
    { "Project" = var.main_tag },
    { "Owner" = var.owner_tag },
  )
  depends_on = [aws_internet_gateway.igw]
}

# NAT Gateway
resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.eip.id
  subnet_id     = aws_subnet.public.0.id
  tags = merge(
    { "Name" = "${var.main_tag}-NAT" },
    { "Project" = var.main_tag },
    { "Owner" = var.owner_tag },
  )
  depends_on = [
    aws_internet_gateway.igw,
    aws_eip.eip
  ]
}

# Public Route Table
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.vpc.id
  tags = merge(
    { "Name" = "${var.main_tag}-routetable" },
    { "Project" = var.main_tag },
    { "Owner" = var.owner_tag },
  )
}

# Public routes
resource "aws_route" "public_internet_access" {
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id
}

# Private Route Table
resource "aws_route_table" "private" {
  vpc_id = aws_vpc.vpc.id
  tags = merge(
    { "Name" = "${var.main_tag}-routetable" },
    { "Project" = var.main_tag },
    { "Owner" = var.owner_tag },
  )
}

# Private Routes
resource "aws_route" "private_internet_access" {
  route_table_id         = aws_route_table.private.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat.id
}

# Public Subnets
resource "aws_subnet" "public" {
  count                   = var.public_subnet_count
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = cidrsubnet(var.vpc_cidr, 4, count.index)
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true
  tags = merge(
    { "Name" = "${var.main_tag}-public-subnet-${data.aws_availability_zones.available.names[count.index]}" },
    { "Project" = var.main_tag },
    { "Owner" = var.owner_tag },
  )
}

# Public Subnet Route Associations
resource "aws_route_table_association" "public" {
  count          = var.public_subnet_count
  subnet_id      = element(aws_subnet.public.*.id, count.index)
  route_table_id = aws_route_table.public.id
}

# Private Subnets
resource "aws_subnet" "private" {
  count             = var.private_subnet_count
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = cidrsubnet(var.vpc_cidr, 4, count.index + var.public_subnet_count)
  availability_zone = data.aws_availability_zones.available.names[count.index]

  tags = merge(
    { "Name" = "${var.main_tag}-private-subnet-${data.aws_availability_zones.available.names[count.index]}" },
    { "Project" = var.main_tag },
    { "Owner" = var.owner_tag },
  )
}

# Private Subnet Route Associations
resource "aws_route_table_association" "private" {
  count = var.private_subnet_count

  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = aws_route_table.private.id
}







