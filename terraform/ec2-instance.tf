
resource "aws_instance" "main" {
  ami                         = var.ami_id
  instance_type               = var.server_instance_type
  key_name                    = var.ec2_key_pair_name
  vpc_security_group_ids      = [aws_security_group.main.id]
  subnet_id                   = aws_subnet.public[0].id
  associate_public_ip_address = true

  tags = merge(
    { "Name" = "${var.main_tag}-controller" },
    { "Project" = var.main_tag },
    { "Owner" = var.owner_tag },
  )
}

# consul server instance
resource "aws_instance" "consul_server" {
  count                  = var.server_desired_count
  ami                    = var.ami_id
  instance_type          = var.server_instance_type
  vpc_security_group_ids = [aws_security_group.consul_server.id]
  subnet_id              = aws_subnet.private[count.index].id
  key_name               = var.ec2_key_pair_name
  tags = merge(
    { "Name" = "${var.main_tag}-server" },
    { "Project" = var.main_tag },
    { "Owner" = var.owner_tag },
    { "Type" = var.server_tag },
  )
  depends_on = [aws_nat_gateway.nat]
}

# consul client instance for api
resource "aws_instance" "consul_client_api" {
  count                  = var.client_desired_count
  ami                    = var.ami_id
  instance_type          = var.client_instance_type
  vpc_security_group_ids = [aws_security_group.consul_client.id]
  subnet_id              = aws_subnet.private[count.index].id
  key_name               = var.ec2_key_pair_name
  tags = merge(
    { "Name" = "${var.main_tag}-client" },
    { "Project" = var.main_tag },
    { "Owner" = var.owner_tag },
    { "Type" = var.client_tag },
    { " Service" = var.api_service_tag },

  )
  depends_on = [aws_nat_gateway.nat]
}

# consul client instance for web
resource "aws_instance" "consul_client_web" {
  count                  = var.client_desired_count
  ami                    = var.ami_id
  instance_type          = var.client_instance_type
  vpc_security_group_ids = [aws_security_group.consul_client.id]
  subnet_id              = aws_subnet.private[count.index].id
  key_name               = var.ec2_key_pair_name
  tags = merge(
    { "Name" = "${var.main_tag}-client" },
    { "Project" = var.main_tag },
    { "Owner" = var.owner_tag },
    { "Type" = var.client_tag },
    { "Service" = var.web_service_tag },
  )
  depends_on = [aws_nat_gateway.nat]
}