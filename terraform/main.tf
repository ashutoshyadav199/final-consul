terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}
# Configure the AWS Provider
provider "aws" {
  region = var.aws_default_region
}

# Details of VPC to be peered
data "aws_vpc" "vpc" {
  state = "available"
  tags  = var.existing_vpc_tags
}

# Details of existing Route table
data "aws_route_table" "routetable" {
  tags = var.existing_routetable_tags
}

# Owner details
data "aws_caller_identity" "owner" {
}

data "aws_security_group" "securitygroup" {
  vpc_id = data.aws_vpc.vpc.id
  tags = {
    "Name" = "my-sg"
  }
}

# VPC peering
resource "aws_vpc_peering_connection" "vpc" {
  peer_owner_id = data.aws_caller_identity.owner.account_id
  peer_vpc_id   = data.aws_vpc.vpc.id
  vpc_id        = aws_vpc.vpc.id
  auto_accept   = true

}
resource "aws_route" "peering_1" {
  route_table_id            = aws_route_table.private.id
  destination_cidr_block    = data.aws_vpc.vpc.cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.vpc.id
}

resource "aws_route" "peering_2" {
  route_table_id            = data.aws_route_table.routetable.id
  destination_cidr_block    = var.vpc_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.vpc.id
}

data "aws_availability_zones" "available" {
  state = "available"
  filter {
    name   = "group-name"
    values = [var.aws_default_region]
  }
}



