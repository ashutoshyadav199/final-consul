variable "main_tag" {
  type        = string
  description = "main project tag"
  default     = "main-project"
}
variable "igw_tags" {
  type        = map(string)
  description = "All tags for igw"
  default = {
    "type" = "dev"
  }
}
variable "owner_tag" {
  type        = string
  description = "owner of resources"
  default     = ""
}
variable "vpc_cidr" {
  type        = string
  description = "CIDR block for vpc"
  default     = ""
}
variable "public_subnet_count" {
  type        = number
  description = "Number of subnet to create"
  default     = 3
}
variable "private_subnet_count" {
  type        = number
  description = "Number of subnet to create"
  default     = 3
}
variable "aws_default_region" {
  type        = string
  description = "Region in which all infra will be created"
  default     = ""
}
variable "ec2_key_pair_name" {
  type        = string
  description = "Name of key pair"
  default     = ""
}
variable "allowed_main_cidr_blocks" {
  type        = list(any)
  description = "cidr_blocks to allowed by main instance"
  default     = ["0.0.0.0/0"]
}
variable "ami_id" {
  type        = string
  description = "ami id for ubuntu 22.04"
  default     = "ami-08c40ec9ead489470"
}
variable "server_desired_count" {
  type        = number
  description = "Number of consul servers should be not more than 3"
  default     = 1
}
variable "allowed_traffic_cidr_blocks" {
  type        = list(any)
  description = "CIDR block for incoming traffic"
  default     = ["0.0.0.0/0"]
}
variable "existing_vpc_tags" {
  type        = map(string)
  description = "tags of existing vpc"
  default     = {}
}
variable "existing_routetable_tags" {
  type        = map(string)
  description = "existing routetable tags"
  default     = {}
}
variable "client_desired_count" {
  type        = number
  description = "Number of node for consul client"
  default     = 1
}
variable "client_instance_type" {
  type        = string
  description = "type of instance for consul client"
  default     = "t2.micro"
}
variable "server_instance_type" {
  type        = string
  description = "Instance type for consul server"
  default     = "t2.micro"
}
variable "server_tag" {
  type        = string
  description = "Tag for specific  consul server"
  default     = "server"
}
variable "client_tag" {
  type        = string
  description = "Tag for specific consul client"
  default     = "client"
}
variable "api_service_tag" {
  type        = string
  description = "tag for api service "
  default     = "api"
}
variable "web_service_tag" {
  type        = string
  description = "tag for web service "
  default     = "web"
}