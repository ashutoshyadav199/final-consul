## Main security group
resource "aws_security_group" "main" {
  name        = "${var.main_tag}-main-sg"
  description = "security group for main instance"
  vpc_id      = aws_vpc.vpc.id
  tags = merge(
    { "Name" = "${var.main_tag}-sg" },
    { "Project" = var.main_tag },
    { "Owner" = var.owner_tag },
  )
}

resource "aws_security_group_rule" "main_allow_22" {
  security_group_id = aws_security_group.main.id
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 22
  to_port           = 22
  cidr_blocks       = var.allowed_main_cidr_blocks
  description       = "Allow SSH traffic."
}

resource "aws_security_group_rule" "main_allow_outbound" {
  security_group_id = aws_security_group.main.id
  type              = "egress"
  protocol          = "-1"
  from_port         = 0
  to_port           = 0
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow any outbound traffic."
}

# Consul Server Security Group
resource "aws_security_group" "consul_server" {
  name        = "${var.main_tag}-consul-server-sg"
  description = "Firewall for the consul server."
  vpc_id      = aws_vpc.vpc.id
  tags = merge(
    { "Name" = "${var.main_tag}-sg" },
    { "Project" = var.main_tag },
    { "Owner" = var.owner_tag },
  )
}
resource "aws_security_group_rule" "consul_server_allow_8500" {
  security_group_id        = aws_security_group.consul_server.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 8500
  to_port                  = 8500
  source_security_group_id = aws_security_group.load_balancer.id
  description              = "Allow HTTP traffic from Load Balancer."
}

resource "aws_security_group_rule" "consul_server_allow_client_8500" {
  security_group_id        = aws_security_group.consul_server.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 8500
  to_port                  = 8500
  source_security_group_id = aws_security_group.consul_client.id
  description              = "Allow HTTP traffic from Consul Client."
}

resource "aws_security_group_rule" "consul_server_allow_client_8301_tcp" {
  security_group_id        = aws_security_group.consul_server.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 8301
  to_port                  = 8301
  source_security_group_id = aws_security_group.consul_client.id
  description              = "Allow LAN gossip traffic from Consul Client to Server.  For managing cluster membership for distributed health check of the agents."
}

resource "aws_security_group_rule" "consul_server_allow_client_8301_udp" {
  security_group_id        = aws_security_group.consul_server.id
  type                     = "ingress"
  protocol                 = "udp"
  from_port                = 8301
  to_port                  = 8301
  source_security_group_id = aws_security_group.consul_client.id
  description              = "Allow LAN gossip traffic from Consul Client to Server.  For managing cluster membership for distributed health check of the agents."
}
resource "aws_security_group_rule" "consul_server_allow_client_8300" {
  security_group_id        = aws_security_group.consul_server.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 8300
  to_port                  = 8300
  source_security_group_id = aws_security_group.consul_client.id
  description              = "Allow RPC traffic from Consul Client to Server.  For client and server agents to send and receive data stored in Consul."
}

resource "aws_security_group_rule" "consul_server_allow_server_8301_tcp" {
  security_group_id        = aws_security_group.consul_server.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 8301
  to_port                  = 8301
  source_security_group_id = aws_security_group.consul_server.id
  description              = "Allow LAN gossip traffic from Consul Server to Server.  For managing cluster membership for distributed health check of the agents."
}

resource "aws_security_group_rule" "consul_server_allow_server_8301_udp" {
  security_group_id        = aws_security_group.consul_server.id
  type                     = "ingress"
  protocol                 = "udp"
  from_port                = 8301
  to_port                  = 8301
  source_security_group_id = aws_security_group.consul_server.id
  description              = "Allow LAN gossip traffic from Consul Server to Server.  For managing cluster membership for distributed health check of the agents."
}

resource "aws_security_group_rule" "consul_server_allow_server_8300" {
  security_group_id        = aws_security_group.consul_server.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 8300
  to_port                  = 8300
  source_security_group_id = aws_security_group.consul_server.id
  description              = "Allow RPC traffic from Consul Server to Server.  For client and server agents to send and receive data stored in Consul."
}

resource "aws_security_group_rule" "consul_server_allow_server_8302_udp" {
  security_group_id        = aws_security_group.consul_server.id
  type                     = "ingress"
  protocol                 = "udp"
  from_port                = 8302
  to_port                  = 8302
  source_security_group_id = aws_security_group.consul_server.id
  description              = "Allow RPC traffic from Consul Server to Server.  For client and server agents to send and receive data stored in Consul."
}

resource "aws_security_group_rule" "consul_server_allow_server_8302_tcp" {
  security_group_id        = aws_security_group.consul_server.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 8302
  to_port                  = 8302
  source_security_group_id = aws_security_group.consul_server.id
  description              = "Allow RPC traffic from Consul Server to Server.  For client and server agents to send and receive data stored in Consul."
}

resource "aws_security_group_rule" "consul_server_allow_22_controller" {
  security_group_id        = aws_security_group.consul_server.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 22
  to_port                  = 22
  source_security_group_id = aws_security_group.main.id
  description              = "Allow SSH traffic from consul bastion."
}
# vpc peering security group
resource "aws_security_group_rule" "vpc_peering" {
  security_group_id        = aws_security_group.consul_server.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 22
  to_port                  = 22
  source_security_group_id = data.aws_security_group.securitygroup.id
  description              = "Allow SSH traffic from consul controller."
}

resource "aws_security_group_rule" "consul_server_allow_outbound" {
  security_group_id = aws_security_group.consul_server.id
  type              = "egress"
  protocol          = "-1"
  from_port         = 0
  to_port           = 0
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow any outbound traffic."
}

# Consul client Security Group
resource "aws_security_group" "consul_client" {
  name        = "${var.main_tag}-consul-client-sg"
  description = "Firewall for the consul client."
  vpc_id      = aws_vpc.vpc.id
  tags = merge(
    { "Name" = "${var.main_tag}-sg" },
    { "Project" = var.main_tag },
    { "Owner" = var.owner_tag },
  )
}

resource "aws_security_group_rule" "consul_client_allow_8500" {
  security_group_id        = aws_security_group.consul_client.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 8500
  to_port                  = 8500
  source_security_group_id = aws_security_group.load_balancer.id
  description              = "Allow HTTP traffic from Load Balancer."
}

# vpc peering security group
resource "aws_security_group_rule" "vpc_peering_client" {
  security_group_id        = aws_security_group.consul_client.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 22
  to_port                  = 22
  source_security_group_id = data.aws_security_group.securitygroup.id
  description              = "Allow SSH traffic from consul controller."
}
resource "aws_security_group_rule" "consul_client_allow_lb_9090" {
  security_group_id        = aws_security_group.consul_client.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 9090
  to_port                  = 9090
  source_security_group_id = aws_security_group.load_balancer.id
  description              = "Allow traffic from Load Balancer to Consul Clients for Fake Service."
}

resource "aws_security_group_rule" "consul_client_allow_9090" {
  security_group_id        = aws_security_group.consul_client.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 9090
  to_port                  = 9090
  source_security_group_id = aws_security_group.consul_client.id
  description              = "Allow traffic from Consul Clients for Fake Service."
}

resource "aws_security_group_rule" "consul_client_allow_8301_tcp" {
  security_group_id        = aws_security_group.consul_client.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 8301
  to_port                  = 8301
  source_security_group_id = aws_security_group.consul_client.id
  description              = "Allow gossip traffic from Consul Clients to Consul Clients."
}

resource "aws_security_group_rule" "consul_client_allow_8301_udp" {
  security_group_id        = aws_security_group.consul_client.id
  type                     = "ingress"
  protocol                 = "udp"
  from_port                = 8301
  to_port                  = 8301
  source_security_group_id = aws_security_group.consul_client.id
  description              = "Allow gossip traffic from Consul Clients to Consul Clients."
}

resource "aws_security_group_rule" "consul_client_allow_consul_server_8301_tcp" {
  security_group_id        = aws_security_group.consul_client.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 8301
  to_port                  = 8301
  source_security_group_id = aws_security_group.consul_server.id
  description              = "Allow gossip traffic from Consul Servers to Consul Clients."
}

resource "aws_security_group_rule" "consul_client_allow_consul_server_8301_udp" {
  security_group_id        = aws_security_group.consul_client.id
  type                     = "ingress"
  protocol                 = "udp"
  from_port                = 8301
  to_port                  = 8301
  source_security_group_id = aws_security_group.consul_server.id
  description              = "Allow gossip traffic from Consul Servers to Consul Clients."
}

resource "aws_security_group_rule" "consul_client_allow_20000" {
  security_group_id        = aws_security_group.consul_client.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 20000
  to_port                  = 20000
  source_security_group_id = aws_security_group.consul_client.id
  description              = "Allow traffic from Consul Clients for Fake Service via Envoy Proxy."
}

resource "aws_security_group_rule" "consul_client_allow_22_bastion" {
  security_group_id        = aws_security_group.consul_client.id
  type                     = "ingress"
  protocol                 = "tcp"
  from_port                = 22
  to_port                  = 22
  source_security_group_id = aws_security_group.main.id
  description              = "Allow SSH traffic from consul bastion."
}

resource "aws_security_group_rule" "consul_client_allow_outbound" {
  security_group_id = aws_security_group.consul_client.id
  type              = "egress"
  protocol          = "-1"
  from_port         = 0
  to_port           = 0
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow any outbound traffic."
}

# Load Balancer SG
resource "aws_security_group" "load_balancer" {
  name        = "${var.main_tag}-alb-sg"
  description = "Firewall for the application load balancer fronting the consul server."
  vpc_id      = aws_vpc.vpc.id
  tags = merge(
    { "Name" = "${var.main_tag}-sg" },
    { "Project" = var.main_tag },
    { "Owner" = var.owner_tag },
  )
}

resource "aws_security_group_rule" "load_balancer_allow_80" {
  security_group_id = aws_security_group.load_balancer.id
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 80
  to_port           = 80
  cidr_blocks       = var.allowed_traffic_cidr_blocks
  description       = "Allow HTTP traffic."
}

resource "aws_security_group_rule" "load_balancer_allow_443" {
  security_group_id = aws_security_group.load_balancer.id
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 443
  to_port           = 443
  cidr_blocks       = var.allowed_traffic_cidr_blocks
  description       = "Allow HTTPS traffic."
}

resource "aws_security_group_rule" "load_balancer_allow_outbound" {
  security_group_id = aws_security_group.load_balancer.id
  type              = "egress"
  protocol          = "-1"
  from_port         = 0
  to_port           = 0
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow any outbound traffic."
}
